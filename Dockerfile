FROM alpine:latest

ENV TERRAFORM_VERSION 0.10.6

RUN apk add --update wget ca-certificates unzip python py-pip openssl bash && \
    apk --update add --virtual build-dependencies python-dev libffi-dev openssl-dev build-base && \
    pip install --upgrade pip cffi && \
    pip install ansible==2.4 && \
    wget -q -O /terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
    unzip /terraform.zip -d /bin && \
    apk del build-dependencies && \
    rm -rf /var/cache/apk/* /terraform.zip && \
    adduser -D -u 497 admin && \
    adduser -D -u 498 admin_user

ENTRYPOINT ["bash"]
